# react-mobx-app-boilerplate

Uses React as view and Mobx as state management

### other technologies:

    - jest
    - storybook
    - parcel-bundler
    - axios
    - @material-ui/styles

### VS Code plugins for a great dev experience

    - prettier
