import { configure } from '@storybook/react'
import requireContext from 'require-context.macro'
import { addDecorator } from '@storybook/react'
import { withConsole } from '@storybook/addon-console'
import { addParameters } from '@storybook/react'

import { jss } from 'react-jss'
import jssPluginCombine from 'jss-plugin-combine'

jss.use(jssPluginCombine())

addParameters({
    i18n: {
        supportedLocales: ['en', 'ar'],
        providerLocaleKey: 'locale'
    }
})

addDecorator((storyFn, context) => withConsole()(storyFn)(context))

const req = requireContext('../src', true, /stories.js$/)

function loadStories() {
    req.keys().forEach(req)
}
configure(loadStories, module)
