module.exports = ({ config }) => {
    config.module.rules.push({
        test: /\.(jsx|js)$/,
        exclude: /(node_modules|bower_components)/,
        loader: require.resolve('babel-loader')
    })
    config.resolve.extensions.push('.jsx', '.js')
    return config
}
