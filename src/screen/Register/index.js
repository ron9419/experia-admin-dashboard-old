import React from 'react'
import Button from '@atom/Button'
import { Link } from 'react-router-dom'
import auth from '@module/auth'

export default () => (
  <div>
    <div>Login</div>
    <Button onClick={() => auth.updateToken('my-token')}>Register</Button>
    <Link to={'/login'}>Login</Link>
  </div>
)
