import React from 'react'
import Button from '@atom/Button'
import { Link } from 'react-router-dom'
import auth from '@module/auth'
import { observer } from 'mobx-react'

export default observer(() => (
  <div>
    <div>Login</div>
    <Button onClick={() => auth.updateToken('my-token')}>Login</Button>
    <input
      value={auth.token}
      onChange={e => auth.updateToken(e.target.value)}
    />
    <Link to={'/register'}>Register</Link>
  </div>
))
