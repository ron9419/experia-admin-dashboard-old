import React from 'react'
import Button from '@atom/Button'
import { Link } from 'react-router-dom'
import auth from '@module/auth'

export default () => (
  <div>
    <div>Home</div>
    <div>
      <Button onClick={() => auth.updateToken(null)}>Logout!</Button>
    </div>
    <Link to={'/login'}>Login</Link>
  </div>
)
