import React from 'react'
import { createLoadable } from '@utils/loadableComponent'
import { Route, Switch, Redirect } from 'react-router'
import { observer } from 'mobx-react'
import auth from '@module/auth'

const loadable = createLoadable({
  minDelay: 400,
  loader: () => <div>Loading...</div>,
})

const HomeScreen = loadable(() => import('./Home'))
const LoginScreen = loadable(() => import('./Login'))
const RegisterScreen = loadable(() => import('./Register'))

export default observer(() =>
  auth.isLoggedIn ? (
    <Switch>
      <Route path={'/home'} component={HomeScreen} />
      <Redirect to={'/home'} />
    </Switch>
  ) : (
    <Switch>
      <Route path={'/login'} component={LoginScreen} />
      <Route path={'/register'} component={RegisterScreen} />
      <Redirect to={'/login'} />
    </Switch>
  )
)
