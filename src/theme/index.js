import createMuiTheme from '@material-ui/core/styles/createMuiTheme'

const theme = createMuiTheme({
  sampleStatus: {
    danger: 'red',
  },
  buttonSize: {
    sm: {
      padding: 20,
    },
    md: {
      padding: 40,
    },
  },
})

export default theme
