import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'

storiesOf('Sample', module).add('button', () => (
  <button onClick={action('clicked')}>
    <span role="img" aria-label="so cool">
      {'Click this button'}
    </span>
  </button>
))
