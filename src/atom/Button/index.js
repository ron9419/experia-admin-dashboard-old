import React from 'react'
import { withStyles } from '@material-ui/styles'

export default withStyles(theme => ({
  button: {
    color: 'red',
    width: 100,
    padding: 20,
    [theme.breakpoints.down('sm')]: {
      width: 50,
      padding: 5,
      color: 'green',
    },
  },
}))(({ children, classes, onClick }) => (
  <button className={classes.button} {...{ onClick }}>
    {children}
  </button>
))
