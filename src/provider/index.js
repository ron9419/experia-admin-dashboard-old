import React from 'react'
import RouterProvider from '@provider/RouterProvider'
import ThemeProvider from '@provider/ThemeProvider'
import theme from '@theme'
import { PersistGate } from '@utils/store/persist'
import authStore from '@module/auth'

// @ts-ignore
const hydrations = [authStore.hydrate]

export default ({ children }) => (
  <PersistGate hydrations={hydrations} loader={'Loading...'}>
    <ThemeProvider theme={theme}>
      <RouterProvider>{children}</RouterProvider>
    </ThemeProvider>
  </PersistGate>
)
