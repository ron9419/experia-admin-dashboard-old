import React from 'react'
import ThemeProvider from '@material-ui/styles/ThemeProvider'

export default ({ children, theme }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
)
