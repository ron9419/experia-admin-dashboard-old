import React from 'react'
import { createBrowserHistory } from 'history'
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router'
import { Router } from 'react-router'

const browserHistory = createBrowserHistory()

export const store = new RouterStore()
export const history = syncHistoryWithStore(browserHistory, store)

export default props => <Router {...props} {...{ history }} />
