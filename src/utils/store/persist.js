import { useState, useEffect } from 'react'
import { autorun, action } from 'mobx'
import objPath from '@utils/objPath'
import isObject from '@utils/isObject'
import pathToObj from '@utils/pathToObj'
import fuseObj from '@utils/fuseObj'
import delay from '@utils/delay'

const accessAllProps = state => {
  if (isObject(state)) {
    for (let p in state) {
      accessAllProps(state[p])
    }
  }
}

const localStorageDriver = {
  setItem: (key, value) => localStorage.setItem(key, value),
  getItem: key => localStorage.getItem(key),
}
const defaultOnStore = state => state
const defaultOnRead = state => JSON.parse(state)
const defaultTransform = state => JSON.stringify(state)

const persist = (
  namespace,
  store,
  {
    keys = [], // the keys of state to be persisted. nested state can be referenced by dot Ex. ['profile.name', 'profile.username']
    migrations = [], // array of migrations Ex. {migrations : [state => ({...state, version: 1}), state => ({...state, version: 2}) ]}
    storage = localStorageDriver,
    onStore = defaultOnStore, // before storing
    onRead = defaultOnRead, // after reading from storage
    transform = defaultTransform, // function used to serialize state
    debounce = 0, // debounce saving to storage if changes are frequent
  } = {}
) => {
  let delayPromise

  autorun(() => {
    keys.forEach(key => {
      const state = objPath(key, store)
      accessAllProps(state)
    })

    if (!store._hyd) {
      return false
    }

    ;(async function() {
      if (delayPromise) delayPromise.cancelDelay()

      delayPromise = delay(debounce)

      if ((await delayPromise).isCancelled) return false

      let state = {}

      keys.forEach(key => {
        state = pathToObj(key.split('.'), state, objPath(key, store))
      })

      state._ver = store._ver || 0

      const transformed = transform(state)

      storage.setItem(namespace, await onStore(transformed))
    })()
  })

  return {
    hydrate: () =>
      new Promise(async resolve => {
        const state = (await onRead(storage.getItem(namespace))) || { _ver: 0 }

        const runMigrations = migrations.slice(state._ver)

        const migratedState = runMigrations.reduce(
          (currentState, migration) => migration(currentState),
          state
        )

        migratedState._ver = migrations.length
        migratedState._hyd = true

        const mapValues = action(() => {
          fuseObj(store, migratedState)
        })

        mapValues()
        resolve()
      }),
  }
}

export default persist

export const PersistGate = ({
  children,
  hydrations = [],
  loader = 'Loading...',
}) => {
  const [isLoaded, setIsLoaded] = useState(false)
  useEffect(() => {
    ;(async function() {
      await Promise.all(hydrations.map(hydrate => hydrate()))

      setIsLoaded(true)
    })()
  }, [])

  return isLoaded ? children : loader
}
