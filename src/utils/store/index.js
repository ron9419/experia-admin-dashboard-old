import { observable, action, flow } from 'mobx'
import persist, { PersistGate } from './persist'
import remotedev from 'mobx-remotedev'
import { createWebApiRequest } from '@utils/webApiRequest'

const separateFunctionsFromObj = obj => {
  const fns = {}

  for (let p in obj) {
    if (obj[p] && obj[p].constructor === Function) {
      fns[p] = obj[p]
    }
  }

  return fns
}

const fnToAction = (namespace, fns) => {
  for (let p in fns) {
    fns[p] = action(`${namespace}/${p}`, fns[p])
  }

  return fns
}

const bindActionToStore = (actions, store) => {
  for (let p in actions) {
    store[p] = actions[p]
  }

  return store
}

export const createWebApiState = (namespace, options) => {
  const webApiRequest = createWebApiRequest(options, namespace)

  const store = observable.object({
    state: 'done', // done, pending, error
    get pending() {
      // @ts-ignore
      return this.state === 'pending'
    },
    error: null,
    data: null,

    request: flow(function*() {
      store.state = 'pending'
      store.error = null

      const { error, data } = yield webApiRequest(...arguments)

      if (error) {
        store.state = 'error'
        store.error = error
      } else {
        store.state = 'done'
        store.data = data
      }
    }),
  })

  return store
}

export const createStore = (namespace, obj = {}, options = {}) => {
  const fns = separateFunctionsFromObj(obj)

  const store = observable.object(obj)

  const actions = fnToAction(namespace, fns)

  const finalStore = bindActionToStore(actions, store)

  if (options.persist) {
    const persistence = persist(namespace, finalStore, options.persist)

    finalStore.hydrate = persistence.hydrate
  }

  remotedev(finalStore, {
    name: namespace,
  })

  return finalStore
}

export { persist, PersistGate }
