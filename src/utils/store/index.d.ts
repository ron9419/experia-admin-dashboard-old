import { IWebApiOptions } from '@utils/webApiRequest'

interface IPersist {
  /**
   * The keys of state to be persisted. nested state can be referenced by dot
   * Ex. ['profile.name', 'profile.username']
   */
  keys: Array<string>

  /**
   * Array of migrations. The version of store will automatically be set
   * Ex. {migrations : [state => ({...state, otherState: 1}), state => ({...state, otherState: 2}) ]}
   */
  migrations?: Array<(state: any) => any>

  /**
   * The storage used for storing data. Default is localStorage
   */
  storage?: {
    setItem: (key: string, value: any) => void
    getItem: (key: string) => any
  }

  /**
   * This will run before storing state. You can use it to encrypt/compress/hash state
   */
  onStore?: (state: any) => string

  /**
   * This will run after reading data from storage. You can use it to decrypt/decompress/checksum state
   */
  onRead?: (state: string) => any

  /**
   * This is the function used to serialize state
   */
  transform?: (state: any) => string

  /**
   * Delay before saving another state. Use it if you persist frequently changing state to optimize the performance
   */
  debounce?: number = 0 // debounce saving to storage if changes are frequent
}
interface IOptions {
  persist?: IPersist
}

export function createStore<T>(
  namespace: string,
  initialState: T,
  options: IOptions
): T

interface IWebApiStore {
  state: 'pending' | 'error' | 'done'
  pending: boolean
  error: any
  data: any
  request: IWebApiOptions.request
}

export function createWebApiState(
  namespace: string,
  options: IWebApiOptions
): IWebApiStore
