const pathToObj = (path, target, value) => {
  switch (path.length) {
    case 0:
      return value
    case 1:
      if (value === undefined) {
        const p = path.shift()
        const newState = {
          ...target,
          [p]: undefined,
        }

        delete newState[p]
        return newState
      }
    default: {
      const p = path.shift()

      return {
        ...target,
        [p]: pathToObj(path, target ? target[p] : {}, value),
      }
    }
  }
}

export default pathToObj
