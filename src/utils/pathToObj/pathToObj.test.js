import pathToObj from './index'

describe('pathToObj util', () => {
  it('overrides the value of the path', () => {
    const state = {
      a: {
        b: 0,
      },
    }

    const newState = pathToObj(['a', 'b'], state, 123)

    expect(newState).toEqual({
      a: {
        b: 123,
      },
    })
  })

  it('preserve other sibling path', () => {
    const state = {
      a: {
        b: 0,
        c: {
          d: 1,
        },
      },
    }

    const newState = pathToObj(['a', 'b'], state, 123)

    expect(newState).toEqual({
      a: {
        b: 123,
        c: {
          d: 1,
        },
      },
    })

    expect(newState.a.c === state.a.c).toBe(true)
  })

  it('overrides path even if object', () => {
    const state = {
      a: {
        b: {
          e: 1,
          f: 1,
        },
        c: {
          d: 1,
        },
      },
    }

    const newState = pathToObj(['a', 'b'], state, { e: 2 })

    expect(newState).toEqual({
      a: {
        b: {
          e: 2,
        },
        c: {
          d: 1,
        },
      },
    })

    expect(newState.a.c === state.a.c).toBe(true)
  })

  it('does not modify original object', () => {
    const state = {
      a: {
        b: {
          e: 1,
          f: 1,
        },
        c: {
          d: 1,
        },
      },
    }

    const newState = pathToObj(['a', 'b'], state, { e: 2 })

    expect(newState).toEqual({
      a: {
        b: {
          e: 2,
        },
        c: {
          d: 1,
        },
      },
    })

    expect(newState.a.c === state.a.c).toBe(true)
    expect(state).toEqual({
      a: {
        b: {
          e: 1,
          f: 1,
        },
        c: {
          d: 1,
        },
      },
    })
  })

  it('deletes path when value is undefined', () => {
    const state = {
      a: {
        b: {
          c: 1,
        },
        d: {},
      },
    }

    const newState = pathToObj(['a', 'b'], state, undefined)

    expect(newState).toEqual({
      a: {
        d: {},
      },
    })
  })

  it('creates path if not exists', () => {
    const state = {
      b: 1,
    }

    const newState = pathToObj(['a', 'b'], state, 1)

    expect(newState).toEqual({
      a: {
        b: 1,
      },
      b: 1,
    })
  })

  it('one path', () => {
    const state = {}

    const newState = pathToObj(['a'], state, 1)

    expect(newState).toEqual({
      a: 1,
    })
  })
})
