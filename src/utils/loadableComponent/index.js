import React, { useState, useEffect, useRef } from 'react'

const LoadableComponent = ({ minDelay = 0, component, loader: Loader }) => {
  const [{ showLoader, LazyComponent }, setState] = useState({
    showLoader: minDelay <= 0,
    LazyComponent: null,
  })

  const timerRef = useRef(null)

  useEffect(() => {
    ;(async function() {
      if (minDelay > 0) {
        timerRef.current = setTimeout(() => {
          setState({
            LazyComponent,
            showLoader: true,
          })
        }, minDelay)
      }

      setState({
        LazyComponent: (await component()).default,
        showLoader: false,
      })

      clearTimeout(timerRef.current)
    })()

    return () => {
      clearTimeout(timerRef.current)
      setState({
        showLoader: minDelay <= 0,
        LazyComponent: null,
      })
    }
  }, [component])

  if (LazyComponent) {
    return <LazyComponent />
  }

  if (showLoader) {
    return Loader ? <Loader /> : <div>{'Loading...'}</div>
  }

  return null
}

export const createLoadable = parentProps => component => () => (
  <LoadableComponent {...parentProps} component={component} />
)
