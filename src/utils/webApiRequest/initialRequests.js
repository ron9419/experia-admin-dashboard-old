const requests = {}

export const cancelRequest = options => {
  const existingDelayPromise = requests[options.id]
  if (existingDelayPromise) {
    existingDelayPromise.cancelDelay()
  }
}

export const addRequest = (options, delayPromise) => {
  cancelRequest(options)
  requests[options.id] = delayPromise
}

export const removeRequest = options => {
  delete requests[options.id]
}
