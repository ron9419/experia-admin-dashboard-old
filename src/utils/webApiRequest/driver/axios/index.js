import axios from 'axios'
import { addRequest, cancelRequest } from './sentRequests'

export default {
  sendRequest: async options => {
    const {
      absoluteURL,
      data,
      headers,
      method,
      params,
      timeout,
      withCredentials,
      adapter,
      auth,
      responseType,
      responseEncoding,
      onUploadProgress,
      onDownloadProgress,
      maxContentLength,
      maxRedirects,
    } = options

    const cancelSource = axios.CancelToken.source()

    addRequest(options, cancelSource)

    // @ts-ignore
    const result = await axios({
      url: absoluteURL,
      method,
      headers,
      data,
      cancelToken: cancelSource.token,
      params,
      timeout,
      withCredentials,
      adapter,
      auth,
      responseType,
      responseEncoding,
      onUploadProgress,
      onDownloadProgress,
      maxContentLength,
      maxRedirects,
    })

    return result
  },
  cancelRequest,
}
