const requests = {}

export const cancelRequest = ({ id, requestId, takeLatest }) => {
  const existingRequests = requests[id]

  if (!existingRequests) return false

  let requestsToCancel = takeLatest
    ? Object.keys(existingRequests)
    : [requestId]

  requestsToCancel.forEach(reqId => {
    existingRequests[reqId].cancelSource.cancel({ isCancelled: true })
    delete existingRequests[reqId]
  })

  if (!Object.keys(existingRequests).length) {
    delete requests[id]
  }
}

export const addRequest = (options, cancelSource) => {
  const existingRequests = requests[options.id]

  requests[options.id] = existingRequests ? existingRequests : {}

  if (requests[options.id][options.requestId]) {
    cancelRequest(options)
  }

  requests[options.id][options.requestId] = { cancelSource }
}
