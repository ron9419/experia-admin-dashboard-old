import axiosDriver from './driver/axios'

let config = {
  method: 'GET',
  baseURL: '',
  tokenResolver: () => '',
  tokenAttacher: (options, token) => {
    options.headers = {
      ...(options.headers || {}),
      Authorization: `Bearer ${token}`,
    }
  },
  tokenRefresher: null,
  timeout: 30000,
  onError: error => error,
  onSuccess: data => data,
  onRequest: () => ({}),
  takeLatest: false,
  retryAttempts: Infinity,
  throttle: 0,
  debounce: 0,
  withToken: false,
  driver: axiosDriver,
}

export const setConfig = mergeConfig => {
  Object.assign(config, mergeConfig)
}

export default config
