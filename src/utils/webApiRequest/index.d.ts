export interface IWebApiOptions {
  url?: string
  method?: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE' = 'GET'
  data?: any
  params?: any
  withToken?: boolean = false
  takeLatest?: boolean = false
  retryAttempts?: number = Infinity
  debounce?: number = 0
  throttle?: number = 0
  baseURL?: string
  timeout?: number = 30000
  tokenResolver?: () => any
  tokenAttacher?: (options: IWebApiOptions, token: string) => void
  onRequest?: (
    data?: any,
    meta?: any
  ) => {
    data?: any
    params?: any
  }
  onError?: (error: any) => any
  onSuccess?: (data: any, response: any) => any
  adapter?: (options) => any
}

export function createWebApiRequest(
  options: IWebApiOptions,
  id: string,
  defaultConfig?: IWebApiOptions
): (...args: Array<any>) => Promise & { cancelRequest: () => void }

export function setConfig(overrideConfig: IWebApiOptions): void

export const config: IWebApiOptions

export default <T>(namespace: string, requestRaw: T) => T
