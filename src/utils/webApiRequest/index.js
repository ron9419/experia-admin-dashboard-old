export { default as config, setConfig } from './config'
export { default as createWebApiRequest } from './createWebApiRequest'
import createWebApiRequests from './createWebApiRequests'

export default createWebApiRequests
