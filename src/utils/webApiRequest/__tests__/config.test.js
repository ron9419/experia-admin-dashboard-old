import config, { setConfig } from "../config";

describe("config", () => {
  it("setConfig works", () => {
    setConfig({
      baseURL: "http://google.com"
    });

    expect(config.baseURL).toBe("http://google.com");
  });
});
