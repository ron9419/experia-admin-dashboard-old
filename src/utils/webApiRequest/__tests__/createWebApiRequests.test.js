import createWebApiRequests from "../createWebApiRequests";

describe("createWebApiRequests", () => {
  it("createWebApiRequests works", () => {
    const requests = createWebApiRequests("my-namespace", {
      getSomething: {
        method: "GET",
        url: "/get-endpoint"
      },
      updateSomething: {
        method: "POST",
        url: "/update-endpoint"
      }
    });

    expect(requests.getSomething.toString()).toBe("my-namespace/getSomething");
    expect(requests.updateSomething.toString()).toBe(
      "my-namespace/updateSomething"
    );

    expect(requests.getSomething).toEqual(expect.any(Function));
    expect(requests.getSomething.cancelRequest).toEqual(expect.any(Function));

    expect(requests.updateSomething).toEqual(expect.any(Function));
    expect(requests.updateSomething.cancelRequest).toEqual(
      expect.any(Function)
    );
  });
});
