import isAbsoluteURL from "../isAbsoluteURL";

describe("isAbsoluteURL", () => {
  describe.each([
    ["http://localhost:9000/sample", true],
    ["https://localhost:9000/sample", true],
    ["/sample", false],
    ["sample", false],
    ["sample.com", false],
    ["sample.com?url=http://locahost", false]
  ])("test cases:", (url, expected) => {
    it(`expected: ${expected} url: ${url}`, () => {
      expect(isAbsoluteURL(url)).toBe(expected);
    });
  });
});
