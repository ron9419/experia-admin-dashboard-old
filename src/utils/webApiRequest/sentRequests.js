const requests = {}

export const addRequest = (options, delayPromise) => {
  const existingDelayPromise = requests[options.id]
  if (existingDelayPromise) {
    existingDelayPromise.cancelDelay()
  }

  requests[options.id] = delayPromise
}

export const removeRequest = options => {
  delete requests[options.id]
}
