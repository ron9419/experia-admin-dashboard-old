import defaultConfig from './config'
import isAbsoluteURL from './isAbsoluteURL'
import * as initialRequests from './initialRequests'
import delay from '@utils/delay'
import fuseObj from '@utils/fuseObj'

const withAbsoluteURL = config => {
  Object.assign(config, {
    absoluteURL: isAbsoluteURL(config.url)
      ? config.url
      : `${config.baseURL}${config.url}`,
  })
}

const withBearerToken = config => {
  if (config.withToken) {
    const token = config.tokenResolver(config)

    config.tokenAttacher(config, token)
  }
}

const debounceFlow = async options => {
  const delayPromise = delay(options.debounce)

  initialRequests.addRequest(options, delayPromise)

  return await delayPromise
}

const onRequestHandler = async options => {
  if (options.takeLatest) {
    options.driver.cancelRequest(options)
  }

  try {
    const result = await options.driver.sendRequest(options)

    if (options.onSuccess) {
      result.data = options.onSuccess(result.data, result)
    }

    return result
  } catch (error) {
    console.error(error)

    return {
      error: options.onError ? options.onError(error) : error,
    }
  }
}

let requestId = 0

export default (options, id, config = defaultConfig) => {
  const webApiRequest = (data, meta) => {
    const requestConfig = { ...config, ...options, id }

    let newOptions = {
      ...requestConfig,
      requestId: ++requestId,
    }

    const promise = (async function() {
      if (newOptions.debounce) {
        if ((await debounceFlow(newOptions)).isCancelled) return
        initialRequests.removeRequest(newOptions)
      }

      const mergeOptions = requestConfig.onRequest(data) // create new config
      fuseObj(newOptions, mergeOptions)

      withAbsoluteURL(newOptions)
      withBearerToken(newOptions)

      return await onRequestHandler(newOptions)
    })()

    promise['cancelRequest'] = () => {
      initialRequests.cancelRequest(newOptions)
      newOptions.driver.cancelRequest(newOptions)
    }

    return promise
  }

  return webApiRequest
}
