import createWebApiRequest from "./createWebApiRequest";

export default (namespace, requestsRaw) =>
  Object.keys(requestsRaw).reduce((requests, requestName) => {
    (requests[requestName] = createWebApiRequest(
      requestsRaw[requestName],
      `${namespace}/${requestName}`
    )).toString = () => `${namespace}/${requestName}`;

    return requests;
  }, {});
