const delay = (timeout = 1000) => {
  let rejector
  let timeoutRef

  const promise = new Promise((resolve, reject) => {
    rejector = reject
    timeoutRef = setTimeout(resolve, timeout)
  })
    .then(() => ({ isCancelled: false }))
    .catch(() => ({ isCancelled: true }))

  promise['cancelDelay'] = () => {
    rejector()
    clearTimeout(timeoutRef)
  }

  return promise
}

export default delay
