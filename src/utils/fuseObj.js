import isObject from '@utils/isObject'

const fuseObj = (target, obj) => {
  for (let p in obj) {
    if (isObject(obj[p])) {
      if (!isObject(target[p])) {
        target[p] = {}
      }

      fuseObj(target[p], obj[p])
    } else {
      target[p] = obj[p]
    }
  }
}

export default fuseObj
