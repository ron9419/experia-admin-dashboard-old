import { setConfig } from '@utils/webApiRequest'
import store from '@module/auth'

setConfig({
  baseURL: 'https://jsonplaceholder.typicode.com',
  tokenResolver: () => store.token,
  timeout: 0,
})
