import React from 'react'
import Screen from '@screen'
import Provider from '@provider'

import { configure } from 'mobx'

configure({
  enforceActions: 'observed',
})

export default () => (
  <Provider>
    <Screen />
  </Provider>
)
