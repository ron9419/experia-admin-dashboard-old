import { createStore, createWebApiState } from '@utils/store'

const store = createStore(
  'auth',
  {
    token: '',

    login: createWebApiState('auth/login', {
      url: '/posts',
      method: 'GET',
      onRequest: data => {
        return {
          params: {
            data,
          },
        }
      },
    }),

    get isLoggedIn() {
      return Boolean(this.token)
    },

    updateToken(value) {
      store.token = value
    },
  },
  {
    persist: {
      keys: ['token'],
    },
  }
)

export default store
