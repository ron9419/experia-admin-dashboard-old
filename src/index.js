// @ts-nocheck
import './init'

import React from 'react'
import { render } from 'react-dom'
import App from './App'

render(<App />, document.getElementById('root'))

import { version } from '../package.json'
console.info(`App version: ${version}`)
