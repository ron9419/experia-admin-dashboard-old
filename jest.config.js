module.exports = {
  verbose: true,
  transform: {
    '^.+\\.(js|ts|tsx)$': 'babel-jest',
  },
  moduleFileExtensions: ['js', 'json', 'ts', 'tsx'],
  // setupFilesAfterEnv: ['jest-enzyme'],
  // testEnvironment: 'enzyme',
  // testEnvironmentOptions: {
  //   enzymeAdapter: 'react16'
  // }
}
